const path = require('path');

// import * as path from 'path'

// import {readFileSync, writeFileSync} from 'fs'

const { readFileSync, writeFileSync } = require('fs');

const lcovFile = path.resolve(__dirname, '../coverage/lcov.info');
const rawFile = readFileSync(lcovFile, 'utf8');
const rebuiltPaths = rawFile
  .split('\n')
  .map((singleLine) => {
    if (singleLine.startsWith('SF:..')) {
      return singleLine.replace('SF:..', `SF:.`);
    }
    return singleLine;
  })
  .join('\n');
// console.log(rebuiltPaths);
writeFileSync(lcovFile, rebuiltPaths, 'utf8');
