import { Inject, Injectable } from '@nestjs/common';
import * as axios from 'axios';
import { AxiosRequestConfig, AxiosResponse } from 'axios';

@Injectable()
export class HttpService {
  private readonly axiosConfig: AxiosRequestConfig;
  private readonly axios;

  constructor(@Inject('CONFIG_OPTIONS') private axiosOptions: AxiosRequestConfig) {
    this.axiosConfig = axiosOptions;
    this.axios = axios.default.create(axiosOptions);
  }

  getConfig(): AxiosRequestConfig {
    return this.axiosConfig;
  }

  async request(config: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.request(config);
  }

  getUri(config?: AxiosRequestConfig): string {
    return this.axios.getUri(config);
  }

  async get(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.get(url, config);
  }

  async delete(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.delete(url, config);
  }

  async head(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.head(url, config);
  }

  async options(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.options(url, config);
  }

  async post(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.post(url, data, config);
  }

  async put(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.put(url, data, config);
  }
  async patch(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.patch(url, data, config);
  }
}
