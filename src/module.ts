import { DynamicModule, Module } from '@nestjs/common';

import { AxiosRequestConfig } from 'axios';
import { HttpService } from './service';

@Module({})
export class HttpModule {
  static forRoot(options?: AxiosRequestConfig): DynamicModule {
    return {
      global: true,
      module: HttpModule,
      providers: [
        HttpService,
        {
          provide: 'CONFIG_OPTIONS',
          useValue: options,
        },
      ],
      exports: [HttpService],      
    };
  }
}
