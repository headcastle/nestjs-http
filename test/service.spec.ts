import * as moxios from 'moxios';

import { Test, TestingModule } from '@nestjs/testing';

import { AxiosRequestConfig } from 'axios';
import { HttpService } from '../src/service';

let service: HttpService;

beforeEach(async () => {
  moxios.install();

  const options: AxiosRequestConfig = { baseURL: 'example.com' };
  const module: TestingModule = await Test.createTestingModule({
    providers: [
      HttpService,
      {
        provide: 'CONFIG_OPTIONS',
        useValue: options,
      },
    ],
  }).compile();

  service = module.get<HttpService>(HttpService);
});

afterEach(async () => {
  moxios.uninstall();
});

it('should be defined', () => {
  expect(service).toBeDefined();
});

it('should return its config', () => {
  expect(service).toBeDefined();
  expect(service.getConfig()).toHaveProperty('baseURL');
  expect(service.getConfig().baseURL).toBe('example.com');
});

it('should return URI', async () => {
  expect(service).toBeDefined();
  expect(service.getUri({ url: '/search', params: { key: 'value' } })).toBe('/search?key=value');
});


it('should GET from url', async () => {
  moxios.stubRequest('/search', {
    status: 200,
    responseText: 'hello',
  });

  const res = await service.request({url:'/search'});
  expect(res.status).toBe(200);
  expect(res.data).toBe('hello');
});

it('should GET from url', async () => {
  moxios.stubRequest('/search', {
    status: 200,
    responseText: 'hello',
  });

  const res = await service.get('/search');
  expect(res.status).toBe(200);
  expect(res.data).toBe('hello');
});

it('should DELETE from url', async () => {
  moxios.stubRequest('/search', {
    status: 200,
    responseText: 'delete',
  });

  const res = await service.delete('/search');
  expect(res.status).toBe(200);
  expect(res.data).toBe('delete');
});

it('should OPTIONS from url', async () => {
  moxios.stubRequest('/search', {
    status: 200,
    responseText: 'options',
  });

  const res = await service.options('/search');
  expect(res.status).toBe(200);
  expect(res.data).toBe('options');
});

it('should HEAD from url', async () => {
  moxios.stubRequest('/search', {
    status: 200,
    responseText: 'head',
  });

  const res = await service.head('/search');
  expect(res.status).toBe(200);
  expect(res.data).toBe('head');
});

it('should POST to url with data', async () => {
  expect.assertions(3);

  const response = {
    status: 200,
    responseText: 'post',
  };

  const sampleData = {
    key: 'value',
  };

  moxios.wait(function () {
    const request = moxios.requests.mostRecent();
    expect(request.config.data).toBe(JSON.stringify(sampleData));
    request.respondWith(response);
  });

  const res = await service.post('/search', sampleData);
  expect(res.status).toBe(200);
  expect(res.data).toBe('post');
});

it('should PUT to url with data', async () => {
  expect.assertions(3);

  const response = {
    status: 200,
    responseText: 'put',
  };

  const sampleData = {
    key: 'value',
  };

  moxios.wait(function () {
    const request = moxios.requests.mostRecent();
    expect(request.config.data).toBe(JSON.stringify(sampleData));
    request.respondWith(response);
  });

  const res = await service.put('/search', sampleData);
  expect(res.status).toBe(200);
  expect(res.data).toBe('put');
});

it('should PATCH to url with data', async () => {
  expect.assertions(3);

  const response = {
    status: 200,
    responseText: 'patch',
  };

  const sampleData = {
    key: 'value',
  };

  moxios.wait(function () {
    const request = moxios.requests.mostRecent();
    expect(request.config.data).toBe(JSON.stringify(sampleData));
    request.respondWith(response);
  });

  const res = await service.patch('/search', sampleData);
  expect(res.status).toBe(200);
  expect(res.data).toBe('patch');
});
