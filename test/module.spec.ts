import { HttpModule } from '../src/module';

it('should be defined', () => {
  expect(HttpModule.forRoot({})).toBeDefined();
});

it('should respect pass configuration', () => {
  const module = HttpModule.forRoot({ baseURL: 'https://examples.com' });
  const providers = module.providers;
  expect(providers).toHaveLength(2);
  expect(module.providers[1]).toHaveProperty('useValue');

  const testProvider = providers[1] as any;
  expect(testProvider.useValue).toStrictEqual({ baseURL: 'https://examples.com' });

  const exports = module.exports;
  expect(exports).toHaveLength(1);
});
