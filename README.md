# nestjs-http

<a href="https://www.npmjs.com/~headcastle"><img src="https://img.shields.io/npm/v/@headcastle/nestjs-http.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~headcastle"><img src="https://img.shields.io/npm/l/@headcastle/nestjs-http.svg" alt="Package License" /></a>
[![pipeline status](https://gitlab.com/headcastle/nestjs-http/badges/main/pipeline.svg)](https://gitlab.com/headcastle/nestjs-http/commits/main)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=headcastle_nestjs-http&metric=security_rating)](https://sonarcloud.io/dashboard?id=headcastle_nestjs-http)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=headcastle_nestjs-http&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=headcastle_nestjs-http)
[![coverage report](https://gitlab.com/headcastle/nestjs-http/badges/main/coverage.svg)](https://gitlab.com/headcastle/nestjs-http/-/commits/main)


## Description

[Axios](https://www.npmjs.com/package/axios) module for [Nest](https://github.com/nestjs/nest). Designed to be a thin wrapper around [Axios](https://github.com/axios/axios) utilizing promises instead of observables.

## Installation

```bash
$ npm i --save @headcastle/nestjs-http
```

## Quick Start

[Overview & Tutorial](https://docs.nestjs.com/techniques/http-module)

## Support

## Stay in touch

## License

Nestjs-http is [MIT licensed](LICENSE).